from . import WaveletDecisionTreeClassifier
from . import wgbm_utils as wgbmu
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn import tree
from .WaveletDecisionTreeClassifier  import WaveletDecisionTreeClassifier
import itertools


class WaveletGBoostingClassifier(tree.DecisionTreeClassifier):

    def __init__(self,
                 base_learner = 'decision_tree',
                 verbose = 1,
                 n_iterations = 100,
                 learning_rate=0.1,
                 min_split_loss=0 ,
                 max_depth=5 ,
                 min_child_weight=1,
                 max_delta_step=0 ,
                 subsample=1,        #Subsample ratio of the training instances.
                 colsample_bylevel=1 ,
                 colsample_bytree=1 ,
                 reg_lambda=1,       #L2 regularization term on weights
                 reg_alpha=0 ,
                 base_score=0,
                 objective='reg:linear' ,
                 scale_pos_weight=1,
                 disable_default_eval_metric = 0,
                 criterion = 'mse' ,
                 max_features = None ,
                 max_leaf_nodes = None ,
                 min_impurity_split = 1e-07 ,
                 min_samples_leaf = 1 ,
                 min_samples_split = 2 ,
                 min_weight_fraction_leaf = 0.0 ,
                 presort = False ,
                 random_state = 1 ,
                 splitter = 'best',
                 wn_th = 0,
                 bag_fraction=0.8,
                 use_wavelates=True,
                 max_prun_precent = 0.5
    ):
        self.criterion = criterion
        self.max_features = max_features
        self.max_leaf_nodes=max_leaf_nodes
        self.min_impurity_split=min_impurity_split = 1e-07
        self.min_samples_leaf = min_samples_leaf
        self.min_samples_split=min_samples_split
        self.min_weight_fraction_leaf = min_weight_fraction_leaf
        self.presort = presort
        self.random_state = random_state
        self.splitter = splitter


        self.base_learner = base_learner
        self.verbose = verbose
        self.n_iterations = n_iterations
        self.learning_rate = learning_rate
        self.min_split_loss = min_split_loss
        self.max_depth = max_depth
        self.min_child_weight = min_child_weight
        self.max_delta_step = max_delta_step
        self.subsample = subsample
        self.colsample_bylevel = colsample_bylevel
        self.colsample_bytree = colsample_bytree
        self.reg_lambda = reg_lambda   # L2 regularization term on weights
        self.reg_alpha = reg_alpha
        self.base_score = base_score
        # self.objective = 'reg:linear' ,
        self.scale_pos_weight = 1
        self.disable_default_eval_metric = 0
        self.wn_th_vec = []

        self.f = []
        self.dtrees = []
        self.f_0 = []
        self.initial_bet = None
        self.bag_fraction = bag_fraction
        np.random.seed (random_state)
        self.vector_of_seeds = np.random.randint(self.n_iterations * 9999, size=self.n_iterations)
        self.use_wavelates = use_wavelates

        self.max_prun_precent = max_prun_precent


    def __compute_best_wn(self,dtree,X,y,max_prun_precent=0.5):
        err_vec=[]
        max_prun_precent = max_prun_precent
        max_n = int(len(dtree.WavelatNorm)*max_prun_precent)

        dtree.WavelatNorm.sort(axis=-1, kind='quicksort', order=None)

        for p in range(0,max_n):
            th =  dtree.WavelatNorm[p]
            y_pred = dtree.predict_wavelet(X,wn_th=th,return_vec=True)
            err_vec.append (wgbmu.MSE_vec(y,y_pred))

        if not err_vec==[]:
            return (dtree.WavelatNorm[err_vec.index(min(err_vec))])
        else :
            return 0


    def fit(self,X,y,initial_bet=None):

        yset = set ( y )
        dim = len ( set ( yset ) ) - 1
        vectors = [ ]
        for d in range ( dim ):
            vec = np.zeros ( dim )
            vec[ d ] = 1
            vectors.append ( vec )
        vec = np.ones ( dim ) * ((1 + np.sqrt ( dim + 1 )) / dim)
        vectors.append ( vec )
        vectors = [ vec.tolist ( ) for vec in vectors ]
        ydict = dict ( )
        for ind in range ( len ( set ( y ) ) ):
            ydict[ yset.pop ( ) ] = ind
        y_conv = [ ]
        for y_samp in y:
            y_conv.append ( vectors[ ydict[ y_samp ] ] )

        self.yset = set ( y )
        self.yvectors = vectors
        self.ydict = ydict
        #
        # print ('y_conv')
        # print (y_conv)
        # print ('wgbmu.most_common(y_conv)')
        # print (wgbmu.most_common(y_conv))
        if initial_bet is None:
            self.initial_bet = wgbmu.most_common(y_conv)
        else:
            self.initial_bet = initial_bet

        for i in range(len(X)):
            self.f_0.append(self.initial_bet)

        self.f.append(self.f_0)
        for k in range(0,self.n_iterations):
            y_k = []
            k+=1
            for y_conv_i , f_km1 in itertools.zip_longest( y_conv , self.f[ k - 1 ] ):

                y_k.append( [u - v for u , v in itertools.zip_longest(y_conv_i,f_km1 )])

            X_train,X_valid,y_k_train,y_k_valid = train_test_split ( X , y_k , test_size=(1-self.bag_fraction) , random_state=self.vector_of_seeds[k-1] )

            dtree = WaveletDecisionTreeClassifier(self.criterion,
                                                 self.max_depth,
                                                 self.max_features,
                                                 self.max_leaf_nodes,
                                                 self.min_impurity_split,
                                                 self.min_samples_leaf,
                                                 self.min_samples_split,
                                                 self.min_weight_fraction_leaf,
                                                 self.presort,
                                                 self.random_state,
                                                 self.splitter)

            dtree.fit(X_train,y_k_train ,already_conved=True)
            self.dtrees.append(dtree)
            if self.use_wavelates:
                self.wn_th_vec.append(self.__compute_best_wn(dtree,X_valid,y_k_valid,self.max_prun_precent))
                prediction_k = dtree.predict_wavelet(X,wn_th=self.wn_th_vec[k-1],return_vec=True)
            else:
                prediction_k = dtree.predict_wavelet ( X , wn_th=0 ,return_vec=True)

            self.f.append ( [prev_k_i + self.learning_rate * prediction_k_i for prev_k_i,prediction_k_i  in itertools.zip_longest(self.f[ k - 1 ],prediction_k)] )

    def predict(self,X):
        y = []
        for i in range(len(X)):
            y.append(self.initial_bet)

        for k in range ( 0 , self.n_iterations ):
            t = self.dtrees[k]
            if self.use_wavelates:
                y_k = t.predict_wavelet(X,wn_th=self.wn_th_vec[k],return_vec=True)
            else:
                y_k = t.predict_wavelet ( X ,wn_th=0,return_vec=True)

            y = [y_i + self.learning_rate * y_k_i for y_k_i,y_i in itertools.zip_longest(y_k,y) ]

        y_pred = [ ]
        for vec in y:
            min_dist = 99999
            cand = None
            dist=-1
            for y_samp in self.yset:
                dist = np.linalg.norm (vec - self.yvectors[ self.ydict[ y_samp ] ] )
                if dist < min_dist:
                    min_dist = dist
                    cand = y_samp
            if cand ==None:
                print("Prediction is too far from yvectors, dist = " +str(dist))
                raise
            else:
                y_pred.append ( cand )
        return y_pred
