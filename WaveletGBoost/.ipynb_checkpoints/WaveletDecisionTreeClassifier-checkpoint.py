from sklearn import tree
from . import wgbm_utils as wgbmu
import numpy as np
import copy

class WaveletDecisionTreeClassifier(tree.DecisionTreeRegressor):
    def __init__(self,criterion='mse', max_depth=None, max_features=None,
                 max_leaf_nodes=None, min_impurity_split=1e-07,
                 min_samples_leaf=1, min_samples_split=2,
                 min_weight_fraction_leaf=0.0, presort=False, random_state=1,
                 splitter='best'):
        ## just like sklearn tree.DecisionTreeClassifier init
        # self.DecisionTreeClassifier = tree.DecisionTreeClassifier()
        super ( tree.DecisionTreeRegressor , self ).__init__ (criterion=criterion, max_depth=max_depth, max_features=max_features,
                 max_leaf_nodes=max_leaf_nodes, min_impurity_split=min_impurity_split,
                 min_samples_leaf=min_samples_leaf, min_samples_split=min_samples_split,
                 min_weight_fraction_leaf=min_weight_fraction_leaf, presort=presort, random_state=random_state,splitter=splitter)


    def fit(self,X,y,sample_weight=None, check_input=True,
            X_idx_sorted=None,already_conved=False):
        if not already_conved:
            yset = set ( y )
            dim = len ( set ( yset ) ) - 1
            vectors = [ ]
            for d in range ( dim ):
                vec = np.zeros ( dim )
                vec[ d ] = 1
                vectors.append ( vec )
            vec = np.ones ( dim ) * ((1 + np.sqrt ( dim + 1 )) / dim)
            vectors.append ( vec )
            vectors = [ vec.tolist ( ) for vec in vectors ]
            ydict = dict ( )
            for ind in range ( len ( set ( y ) ) ):
                ydict[ yset.pop ( ) ] = ind
            y_conv = [ ]
            for y_samp in y:
                y_conv.append ( vectors[ ydict[ y_samp ] ] )
        else:
            y_conv = y

        super ( tree.DecisionTreeRegressor , self ).fit (
            X , y_conv,
            sample_weight=sample_weight ,
            check_input=check_input ,
            X_idx_sorted=X_idx_sorted )
        if not already_conved:

            self.yset = set ( y )
            self.yvectors = vectors
            self.ydict = ydict

        self.WavelatNorm = wgbmu.compute_wavelet_norm(self,X,y_conv)
        # print(self.DecisionTreeClassifier.WavelatNorm )
        n_nodes = self.tree_.node_count
        children_left = self.tree_.children_left
        children_right = self.tree_.children_right
        feature = self.tree_.feature
        threshold = self.tree_.threshold

        [ node_depth , is_leaves ] = wgbmu.depth_and_isleaves_by_node ( n_nodes , children_left , children_right )
        [ node_count , node_sum , node_mean ] = wgbmu.compute_count_sum_and_mean_per_node ( X=X , y=y_conv ,children_right=children_right ,children_left=children_left , n_nodes=n_nodes ,is_leaves=is_leaves,
                                                                                            feature=feature,threshold=threshold)
        self.node_depth = node_depth
        self.is_leaves = is_leaves
        self.node_count = node_count
        self.node_sum = node_sum
        self.node_mean = node_mean
        # print(self.node_mean )
        self.node_parent = wgbmu.compute_nodes_parent(n_nodes=n_nodes,children_left=children_left,children_right=children_right)

        return self


    def predict_standard(self,X):
        y = np.zeros ( shape=len(X), dtype=np.float64 )
        for x_ind in range(len(X)):
            x= X[x_ind]
            node = 0
            while not self.DecisionTreeClassifier.is_leaves[node]:
                if self.DecisionTreeClassifier.tree_.threshold[node] >= x[self.DecisionTreeClassifier.tree_.feature[node]]:
                    node = self.DecisionTreeClassifier.tree_.children_left[ node ]
                else:
                    node = self.DecisionTreeClassifier.tree_.children_right[ node ]
            y[x_ind] = self.DecisionTreeClassifier.node_mean[node]
        return y



    def predict_wavelet(self,X,wn_th=0,return_vec=False):
        y_vec = []
        for x_ind in range ( len ( X ) ):
            y_vec.append(np.zeros(shape=len(self.node_mean[0]),dtype=np.float64))

        for x_ind in range(len(X)):
            x= X[x_ind]
            node = 0
            y_vec[ x_ind ] = copy.copy(self.node_mean[ node ])
            last_r = False
            while not last_r:
                if self.tree_.threshold[node] >= x[self.tree_.feature[node]]:
                    node = self.tree_.children_left[ node ]
                else:
                    node = self.tree_.children_right[ node ]
                if self.tree_.children_left[ node ] == -1 and self.tree_.children_right[ node ] == -1:
                    last_r=True
                if self.WavelatNorm[node] >= wn_th:
                    y_vec[ x_ind ] += self.node_mean[ node ] - \
                                  self.node_mean[
                                      self.node_parent[ node ] ]

        if return_vec:
            return y_vec

        y_pred = [ ]
        for vec in y_vec:
            min_dist = 99999
            cand = None
            dist=-1
            for y_samp in self.yset:
                dist = np.linalg.norm (vec - self.yvectors[ self.ydict[ y_samp ] ] )
                if dist < min_dist:
                    min_dist = dist
                    cand = y_samp
            if cand ==None:
                print("Prediction is too far from yvectors, dist = " +str(dist))
                raise
            else:
                y_pred.append ( cand )
        return y_pred
