import numpy as np
import statistics
import itertools
import operator


def depth_and_isleaves_by_node(n_nodes ,children_left,children_right):
    node_depth = np.zeros ( shape=n_nodes , dtype=np.int64 )
    is_leaves = np.zeros ( shape=n_nodes , dtype=bool )
    stack = [ (0 , -1) ]  # seed is the root node id and its parent depth
    while len ( stack ) > 0:
        node_id , parent_depth = stack.pop ( )
        node_depth[ node_id ] = parent_depth + 1

        # If we have a test node
        if (children_left[ node_id ] != children_right[ node_id ]):
            stack.append ( (children_left[ node_id ] , parent_depth + 1) )
            stack.append ( (children_right[ node_id ] , parent_depth + 1) )
        else:
            is_leaves[ node_id ] = True
    return [node_depth,is_leaves]

def compute_count_sum_and_mean_per_node(X,y,children_right,children_left,n_nodes,is_leaves,threshold,feature):

    if not np.array(y[0]).shape == ():
        node_count = np.zeros ( shape=n_nodes , dtype=np.int64 )
        node_sum=[]
        node_mean=[]
        for n in range(n_nodes):
            node_sum.append(np.zeros ( shape=len(y[0]) , dtype=np.float64 ))
            node_mean.append(np.zeros ( shape=len(y[0]) , dtype=np.float64 ))

        for x_ind in range ( len ( X ) ):
            x = X[ x_ind ]
            node = 0
            while not is_leaves[ node ]:
                node_count[node] += 1
                node_sum[node] += y[x_ind]
                if threshold[ node ] > x[feature[node]]:
                    node = children_left[ node ]
                else:
                    node = children_right[ node ]
            node_count[ node ] += 1
            node_sum[ node ] += y[ x_ind ]

        for j in range(n_nodes):
            node_mean[j] = node_sum[j]/float(node_count[j])
        return [node_count , node_sum , node_mean]
    elif np.array(y[0]).shape == ():
        node_count = np.zeros ( shape=n_nodes , dtype=np.int64 )
        node_sum = np.zeros ( shape=n_nodes , dtype=np.float64 )
        node_mean = np.zeros ( shape=n_nodes , dtype=np.float64 )

        for x_ind in range ( len ( X ) ):
            x = X[ x_ind ]
            node = 0
            while not is_leaves[ node ]:
                node_count[ node ] += 1
                node_sum[ node ] += y[ x_ind ]
                if threshold[ node ] > x[ feature[ node ] ]:
                    node = children_left[ node ]
                else:
                    node = children_right[ node ]
            node_count[ node ] += 1
            node_sum[ node ] += y[ x_ind ]

        for j in range ( n_nodes ):
            node_mean[ j ] = node_sum[ j ] / float ( node_count[ j ] )

        return [ node_count , node_sum , node_mean ]


def compute_nodes_parent(n_nodes,children_left,children_right):
    nodes_parent = np.zeros ( shape=n_nodes , dtype=np.int64 )
    for i in range ( n_nodes ):
        l , = np.where ( children_left == i )
        r , = np.where ( children_right == i )
        if len(l)!= 0 and len(r) != 0:
            raise("error: wrong tree structure")
        if len(l) == 0 and len(r)== 0:
            nodes_parent[i] = -1
        elif len(l)==0:
            nodes_parent[ i ] = r[0]
        elif len(r)==0:
            nodes_parent[ i ] = l[0]
    return nodes_parent

def compute_wavelet_norm(model,X,y):
    n_nodes = model.tree_.node_count
    children_left = model.tree_.children_left
    children_right = model.tree_.children_right
    feature = model.tree_.feature
    threshold = model.tree_.threshold
    [ node_depth , is_leaves ] = depth_and_isleaves_by_node(n_nodes ,children_left,children_right)

    nodes_parent =  compute_nodes_parent(n_nodes=n_nodes,children_left=children_left,children_right=children_right)

    [ node_count , node_sum , node_mean ] = compute_count_sum_and_mean_per_node(X=X,y=y,children_right=children_right,
                                                                                children_left=children_left,n_nodes=n_nodes,is_leaves=is_leaves,
                                                                                threshold=threshold,feature=feature)


    if not np.array(y[0]).shape == ():
        node_norm = np.zeros ( shape=n_nodes , dtype=np.float64 )
        for j in range(n_nodes):
            sum = 0
            for comp in range(len(node_mean[j])):
                sum+=(node_mean[j][comp]-node_mean[nodes_parent[j]][comp])**2
            node_norm[j] = node_count[j]*sum
    elif np.array(y[0]).shape == ():
        node_norm = np.zeros ( shape=n_nodes , dtype=np.float64 )
        for j in range ( n_nodes ):
            node_norm[ j ] = node_count[ j ] * (node_mean[ j ] - node_mean[ nodes_parent[ j ] ]) ** 2

    else:
        print ('Error. Cannot determin np.array(y[0]).shape')
        raise

    return node_norm


def MAD(X,Y):
    X = [ float ( f ) for f in X ]
    Y = [ float ( f ) for f in Y ]
    d = []
    for ind in range ( len ( X ) ):
        d.append(abs(X[ ind ] - Y[ ind ]))
    return  statistics.median(d)

def MSE(X,Y):
    X = [ float ( f ) for f in X ]
    Y = [ float ( f ) for f in Y ]
    d = []
    for ind in range ( len ( X ) ):
        d.append ( (X[ ind ] - Y[ ind ]) ** 2 )
    return  statistics.mean(d)**0.5


def MSE_vec(X,Y):
    d = []
    for ind in range ( len ( X ) ):
        d.append ( np.linalg.norm (X[ ind ]-Y[ ind ]))
    return  statistics.mean(d)**0.5


def most_common(L):
  SL = sorted((x, i) for i, x in enumerate(L))
  groups = itertools.groupby(SL, key=operator.itemgetter(0))
  def _auxfun(g):
    item, iterable = g
    count = 0
    min_index = len(L)
    for _, where in iterable:
      count += 1
      min_index = min(min_index, where)
    return count, -min_index
  return max(groups, key=_auxfun)[0]


def classification_error (X,Y):
    sum_ = sum([x==y for x,y in itertools.zip_longest(X,Y)])
    return sum_/(len(X))