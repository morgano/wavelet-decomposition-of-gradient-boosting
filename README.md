# Wavelet decomposition of Gradient Boosting

Wavelet decomposition of Gradient Boosting is an improvement of the popular tree-based algorithm 'Gradient Boosting', by combine Function Space theory and Harmonic Analysis into it (Geometric Wavelets [ref1])
This project is a python implementation of the WGB algorithm for both Regression and Classification problem, the implementation made by follow (as much as possible) the the implementation of the well know library scikit-learn (sklearn) and compered to the implelmentation of gradient boosting in that library and to XBBoost algorithm [ref2]

In addition you can find three ipython notebooks with examples of how to use this implementation algorithm, a bit deeper explanation of the features and the process of the algorithm, and comparison with XGBoost and sklearn's GradientBoosting.

For more details about the algorithm and the theory beyond it, please read [ref3].
