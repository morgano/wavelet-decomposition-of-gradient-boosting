from os import path
from numpy import loadtxt,genfromtxt

def data_reader(data_path,dataset_name,classification=False):
	X = genfromtxt(path.join(data_path,dataset_name,'trainingData.txt'), delimiter=" ")
	if classification:
		y = genfromtxt ( path.join ( data_path,dataset_name, 'trainingLabel.txt' ),delimiter=" ",dtype=str)
	else:	   
		y = genfromtxt ( path.join ( data_path,dataset_name, 'trainingLabel.txt' ),delimiter=" ")
	return X,y
